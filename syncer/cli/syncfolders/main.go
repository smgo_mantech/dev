package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"path/filepath"
	"strconv"
	"syscall"

	"github.com/syncthing/syncthing/lib/config"

	"bitbucket.org/smgo_mantech/dev/syncer/lib/syncthing"

	"github.com/syncthing/syncthing/lib/events"

	"github.com/thejerf/suture"
)

// 하나의 장비에서 두 폴더를 동기화 하는 실험

var (
	stop       = make(chan int)
	sourcePort = 9998
	targetPort = 9999
	folderName = "f1"
)

func setupSignalHandling() {
	stopSign := make(chan os.Signal, 1)
	sigTerm := syscall.Signal(15)
	signal.Notify(stopSign, os.Interrupt, sigTerm)
	go func() {
		<-stopSign
		stop <- 0
	}()
}

func makeAddress(port int) string {
	return fmt.Sprintf("tcp://%s", net.JoinHostPort("127.0.0.1", strconv.Itoa(port)))
}

func main() {
	flag.Parse()
	sourcePath := flag.Arg(0)
	targetPath := flag.Arg(1)
	if sourcePath == "" || targetPath == "" {
		fmt.Printf("usage: %s source_path target_path\n", os.Args[0])
		return
	}

	sourcePath, _ = filepath.Abs(sourcePath)
	targetPath, _ = filepath.Abs(targetPath)

	fmt.Printf("source path: %s\n", sourcePath)
	fmt.Printf("target path: %s\n", targetPath)

	// 서비스 생성 및 시작
	mainService := suture.New("main", suture.Spec{
		Log: func(line string) {
			log.Println(line)
		},
		PassThroughPanics: true,
	})
	mainService.ServeBackground()
	setupSignalHandling()

	// Source
	source, err := syncthing.NewController("conf1", sourcePort)
	if err != nil {
		log.Fatalln(err)
		return
	}
	source.Serve(mainService)

	// Target
	target, err := syncthing.NewController("conf2", targetPort)
	if err != nil {
		log.Fatalln(err)
		return
	}
	target.Serve(mainService)

	// Prepare

	// Source config
	targetDevice, err := source.SetDevice(
		target.ID(), "target", makeAddress(targetPort))
	if err != nil {
		log.Fatalln(err)
		return
	}
	source.SetFolder(folderName, sourcePath, []config.DeviceConfiguration{targetDevice})

	// Target config
	sourceDevice, err := target.SetDevice(
		source.ID(), "source", makeAddress(sourcePort))
	if err != nil {
		log.Fatalln(err)
		return
	}
	target.SetFolder(folderName, targetPath, []config.DeviceConfiguration{sourceDevice})

	// 이벤트 구독
	sub := events.Default.Subscribe(events.AllEvents)
	eventChan := sub.C()

	for {
		select {
		case event := <-eventChan:
			j, err := json.Marshal(event)
			if err != nil {
				log.Fatalln(err)
			}
			log.Println(string(j))
		case <-stop:
			return
		}
	}
}
