package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"path/filepath"
	"syscall"

	"bitbucket.org/smgo_mantech/dev/syncer/lib/syncthing"
	"github.com/syncthing/syncthing/lib/config"
	"github.com/syncthing/syncthing/lib/events"
	"github.com/thejerf/suture"
)

var (
	stop = make(chan int)
)

func setupSignalHandling() {
	stopSign := make(chan os.Signal, 1)
	sigTerm := syscall.Signal(15)
	signal.Notify(stopSign, os.Interrupt, sigTerm)
	go func() {
		<-stopSign
		stop <- 0
	}()
}

func main() {
	// 실행 인수 파싱
	var confPath string
	var listenPort int
	var peerID string
	var peerAddress string
	var folderName string

	flag.StringVar(&confPath, "conf", "./conf", "Configuration path")
	flag.IntVar(&listenPort, "port", 9876, "Listen port number")
	flag.StringVar(&peerID, "peer_id", "", "Peer ID")
	flag.StringVar(&peerAddress, "peer_addr", "127.0.0.1:9877", "Peer address")
	flag.StringVar(&folderName, "name", "F1", "Folder name")

	flag.Parse()
	path := flag.Arg(0)
	if path == "" {
		fmt.Printf("usage: %s {options...} folder_path", os.Args[0])
		flag.Usage()
		return
	}

	// 설정 폴더가 없으면 생성
	if _, err := os.Stat(confPath); err == os.ErrNotExist {
		err = os.MkdirAll(confPath, 755)
		if err != nil {
			log.Fatalf("failed to make configuration folder. %v\n", err)
			return
		}
	}

	// 폴더 경로를 절대 경로로 변경
	path, _ = filepath.Abs(path)

	// 서비스 생성 및 시작
	mainService := suture.New("main", suture.Spec{
		Log: func(line string) {
			log.Println(line)
		},
		PassThroughPanics: true,
	})
	mainService.ServeBackground()

	// Syncthing 제어기 생성
	s, err := syncthing.NewController(confPath, listenPort)
	if err != nil {
		log.Fatalln(err)
		return
	}
	// 시작
	s.Serve(mainService)

	log.Printf("MyID: %s\n", s.ID())
	log.Printf("Folder: %s\n", path)

	// 장비 및 폴더 추가
	var peerDevice config.DeviceConfiguration
	if peerID != "" {
		peerDevice, _ = s.SetDevice(peerID, "peer", peerAddress)
	}
	s.SetFolder(folderName, path, []config.DeviceConfiguration{peerDevice})

	// 이벤트 구독
	sub := events.Default.Subscribe(events.AllEvents)
	eventChan := sub.C()

	for {
		select {
		case event := <-eventChan:
			j, err := json.Marshal(event)
			if err != nil {
				log.Fatalln(err)
			}
			log.Println(string(j))
		case <-stop:
			return
		}
	}
}
