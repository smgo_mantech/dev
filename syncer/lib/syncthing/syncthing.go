package syncthing

import (
	"crypto/tls"
	"fmt"
	"net"
	"os"
	"strconv"

	"github.com/syncthing/syncthing/lib/config"
	"github.com/syncthing/syncthing/lib/connections"
	"github.com/syncthing/syncthing/lib/db"
	"github.com/syncthing/syncthing/lib/fs"
	"github.com/syncthing/syncthing/lib/model"
	"github.com/syncthing/syncthing/lib/protocol"
	"github.com/syncthing/syncthing/lib/tlsutil"
	"github.com/thejerf/suture"
)

var (
	bepProtocolName      = "bep/1.0"
	tlsDefaultCommonName = "RepliX"
)

// Controller --
type Controller struct {
	myID     protocol.DeviceID
	confPath string
	cert     tls.Certificate
	cfg      *config.Wrapper
	ldb      *db.Lowlevel

	model       *model.Model
	connections *connections.Service
}

func makeConf(myID protocol.DeviceID, confPath string, port int) *config.Wrapper {
	cfg := config.New(myID)

	hostname, _ := os.Hostname()

	thisDevice := config.NewDeviceConfiguration(myID, hostname)
	thisDevice.Addresses = []string{"dynamic"}

	cfg.Devices = []config.DeviceConfiguration{thisDevice}

	cfg.GUI.RawAddress = fmt.Sprintf("127.0.0.1:%d", 8384)
	cfg.GUI.Enabled = false

	cfg.Options.ListenAddresses = []string{
		fmt.Sprintf("tcp://%s", net.JoinHostPort("0.0.0.0", strconv.Itoa(port))),
	}

	cfg.Options.RelaysEnabled = false
	cfg.Options.DeprecatedUPnPEnabled = false
	cfg.Options.GlobalAnnEnabled = false
	cfg.Options.LocalAnnEnabled = false
	cfg.Options.NATEnabled = false

	return config.Wrap(confPath+"/conf.xml", cfg)
}

// ID -- 현재 장비의 ID
func (c *Controller) ID() string {
	return c.myID.String()
}

// Serve -- syncthing 실행
func (c *Controller) Serve(service *suture.Supervisor) {
	c.model = model.NewModel(c.cfg, c.myID, tlsDefaultCommonName, "", c.ldb, []string{})
	service.Add(c.model)

	// cachedDiscovery := discover.NewCachingMux()
	// service.Add(cachedDiscovery)

	tlsCfg := tlsutil.SecureDefault()
	tlsCfg.Certificates = []tls.Certificate{c.cert}
	tlsCfg.NextProtos = []string{bepProtocolName}
	tlsCfg.ClientAuth = tls.RequestClientCert
	tlsCfg.SessionTicketsDisabled = true
	tlsCfg.InsecureSkipVerify = true

	c.connections = connections.NewService(c.cfg, c.myID, c.model, tlsCfg, nil, bepProtocolName, tlsDefaultCommonName)
	service.Add(c.connections)
}

// SetFolder -- 폴더 설정
func (c *Controller) SetFolder(name string, path string, devices []config.DeviceConfiguration) error {
	folder := config.NewFolderConfiguration(c.myID, name, path, fs.FilesystemTypeBasic, path)
	for _, device := range devices {
		folder.Devices = append(folder.Devices, config.FolderDeviceConfiguration{DeviceID: device.DeviceID})
	}

	folder.CreateRoot()

	_, err := c.cfg.SetFolder(folder)
	return err
}

// SetDevice -- 장비 설정
func (c *Controller) SetDevice(id string, name string, address string) (config.DeviceConfiguration, error) {
	deviceID, err := protocol.DeviceIDFromString(id)
	if err != nil {
		return config.DeviceConfiguration{}, err
	}
	device := config.NewDeviceConfiguration(deviceID, name)
	device.Addresses = []string{address}

	_, err = c.cfg.SetDevice(device)
	return device, err
}

// NewController -- 제어기 생성
func NewController(confPath string, port int) (*Controller, error) {
	certFile := confPath + "/cert.pem"
	keyFile := confPath + "/key.pem"

	// Ensure that we have a certificate and key.
	cert, err := tls.LoadX509KeyPair(certFile, keyFile)
	if err != nil {
		fmt.Printf("Generating ECDSA key and certificate for %s...\n", tlsDefaultCommonName)
		cert, err = tlsutil.NewCertificate(certFile, keyFile, tlsDefaultCommonName)
		if err != nil {
			fmt.Printf("%v\n", err)
		}
	}

	ldb, err := db.Open(confPath + "/index.db")
	if err != nil {
		fmt.Printf("Error opening database: %v\n", err)
	}
	if err := db.UpdateSchema(ldb); err != nil {
		fmt.Printf("Database schema: %v\n", err)
	}

	myID := protocol.NewDeviceID(cert.Certificate[0])

	controller := Controller{
		myID:     myID,
		confPath: confPath,
		cert:     cert,
		cfg:      makeConf(myID, confPath, port),
		ldb:      ldb,
	}

	return &controller, nil
}
