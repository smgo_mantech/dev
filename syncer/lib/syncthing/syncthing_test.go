package syncthing

import (
	"testing"
	"time"

	"github.com/thejerf/suture"
)

func TestService(t *testing.T) {
	s, err := NewController("./conf", 9876)
	if err != nil {
		t.Error(err)
		return
	}

	mainService := suture.New("main", suture.Spec{
		Log: func(line string) {
			t.Log(line)
		},
		PassThroughPanics: true,
	})
	mainService.ServeBackground()
	s.Serve(mainService)

	time.Sleep(10 * time.Second)

	mainService.Stop()
}
